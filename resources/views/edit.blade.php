@extends('layouts.main')
@section('content')
<div class="container">
<br>

@if($errors->any())
@foreach($errors->all() as $error)
  <div class="alert alert-danger" role="alert">
    {{ $error }}
  </div>
@endforeach
@endif

<h1>Edit Page</h1>
<!-- Default form register -->
<form class="text-center border border-light p-5" method="POST" action="{{ route('update', $student->id) }}">
  {{ csrf_field() }}
    <p class="h4 mb-4">Add Student</p>

    <div class="form-row mb-4">
        <div class="col">
            <!-- First name -->
            <input type="text" id="defaultRegisterFormFirstName" class="form-control" value="{{ $student->first_name }}" name="first_name" placeholder="First name">
        </div>
        <div class="col">
            <!-- Last name -->
            <input type="text" id="defaultRegisterFormLastName" class="form-control" name="last_name" value="{{ $student->last_name }}" placeholder="Last name">
        </div>
    </div>

    <!-- E-mail -->
    <input type="email" id="defaultRegisterFormEmail" class="form-control mb-4" name="email" value="{{ $student->email }}" placeholder="E-mail">

    <!-- Password -->
    <!-- <input type="password" id="defaultRegisterFormPassword" class="form-control" placeholder="Password" aria-describedby="defaultRegisterFormPasswordHelpBlock">
    <small id="defaultRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
        At least 8 characters and 1 digit
    </small> -->

    <!-- Phone number -->
    <input type="text" id="defaultRegisterPhonePassword" class="form-control" name="phone_no" value="{{ $student->phone_no }}" placeholder="Phone number" aria-describedby="defaultRegisterFormPhoneHelpBlock">
    <!-- <small id="defaultRegisterFormPhoneHelpBlock" class="form-text text-muted mb-4">
        Optional - for two step authentication
    </small> -->

    <!-- Newsletter -->
    <!-- <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="defaultRegisterFormNewsletter">
        <label class="custom-control-label" for="defaultRegisterFormNewsletter">Subscribe to our newsletter</label>
    </div> -->
    
    <!-- Sign up button -->
    <button class="btn btn-info my-4 btn-block" type="submit">Update</button>

    <!-- Social register -->
    <!-- <p>or sign up with:</p>

    <a href="#" class="mx-2" role="button"><i class="fab fa-facebook-f light-blue-text"></i></a>
    <a href="#" class="mx-2" role="button"><i class="fab fa-twitter light-blue-text"></i></a>
    <a href="#" class="mx-2" role="button"><i class="fab fa-linkedin-in light-blue-text"></i></a>
    <a href="#" class="mx-2" role="button"><i class="fab fa-github light-blue-text"></i></a>
    -->
     

    <!-- Terms of service -->
    <!-- <p>By clicking
        <em>SUbmit</em> you agree to our
        <a href="" target="_blank">terms of service</a> -->

</form>
<!-- Default form register -->
<br>
</div>

@endsection
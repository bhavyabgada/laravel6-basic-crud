<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;

class StudentController extends Controller
{
    public function index()
    {
      $students = Students::paginate(5);
      return view('dashboard',compact('students'));
    }
    public function create()
    {
      return view('create');
    }
    public function store(Request $request)
    {
      $this->validate($request,[
        'first_name'  =>  'required',
        'last_name'  =>  'required',
        'email'  =>  'required',
        'phone_no'  =>  'required'
      ]);
      $student = new Students;
      $student->first_name = $request->first_name;
      $student->last_name = $request->last_name;
      $student->email = $request->email;
      $student->phone_no = $request->phone_no;
      $student->save();
      return redirect(route('dashboard'))->with('successMessage','Student Successfully Added');
    }
    public function edit($id)
    {
      $student = Students::find($id);
      return view('edit',compact('student'));
    }
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'first_name'  =>  'required',
        'last_name'  =>  'required',
        'email'  =>  'required',
        'phone_no'  =>  'required'
      ]);
      $student = Students::find($id);
      $student->first_name = $request->first_name;
      $student->last_name = $request->last_name;
      $student->email = $request->email;
      $student->phone_no = $request->phone_no;
      $student->save();
      return redirect(route('dashboard'))->with('successMessage','Student Successfully Updated');
    }
    public function delete($id)
    {
      Students::find($id)->delete();
      return redirect(route('dashboard'))->with('successMessage','Student Deleted Successfully');
    }
}

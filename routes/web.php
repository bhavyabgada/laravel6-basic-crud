<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){
  return view('welcome');
});

//student-crud-routes
Route::get('/dashboard', 'StudentController@index' )->name('dashboard');
Route::get('/create', 'StudentController@create' )->name('create');
Route::post('/store', 'StudentController@store' )->name('store');
Route::get('/edit/{id}', 'StudentController@edit' )->name('edit');
Route::post('/update/{id}', 'StudentController@update' )->name('update');
Route::delete('/delete/{id}', 'StudentController@delete' )->name('delete');

//Auth-routes
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/change-password', 'Auth\ChangePasswordController@index')->name('password.change');
Route::post('/change-password', 'Auth\ChangePasswordController@changepassword')->name('password.update');
